import toolIcon from './ejs-superscript.svg'
import './superscript.css'

//
// refer https://editorjs.io/inline-tool-user-interaction example
//
export default class Superscript {
  static get cssClass () {
    return 'ejs-sup'
  }

  constructor ({ api }) {
    this.api = api
    this.button = null

    this._state = false

    // tag must be UPPERCASE
    this.tag = 'SUP'

    this.isBrowserFirefox = false
    if (/Firefox\//.test(navigator.userAgent)) {
      this.isBrowserFirefox = true
    }
  }

  static get isInline () {
    return true
  }

  static get title () {
    return 'Superscript'
  }

  static get sanitize () {
    return {
      sup: {
        class: Superscript.cssClass
      }
    }
  }

  get state () {
    return this._state
  }

  set state (state) {
    this._state = state
  }

  render () {
    this.button = document.createElement('button')
    this.button.type = 'button'
    this.button.innerHTML = toolIcon
    this.button.classList.add('ce-inline-tool')

    return this.button
  }

  surround (range) {
    if (this.state) {
      this.unwrap(range)
      return
    }

    this.wrap(range)
  }

  wrap (range) {
    const selectedText = range.extractContents()
    const superscript = document.createElement(this.tag)

    superscript.classList.add(Superscript.cssClass)
    superscript.appendChild(selectedText)
    range.insertNode(superscript)

    this.api.selection.expandToTag(superscript)
  }

  unwrap (range) {
    const superscript = this.api.selection.findParentTag(this.tag, Superscript.cssClass)

    const tagContent = superscript.innerText

    if (tagContent === range.toString()) {
      // full selection
      const text = range.extractContents()
      superscript.remove()
      range.insertNode(text)
    } else {
      // partial selection

      // THOUGHT PROCESS
      //
      // superscript text
      //     "again again again"
      //
      // STEP 1 - mark selection
      //     "<span>again</span> again again"
      //
      // STEP 2 - place delimiters for splitting
      //     ",<span>again</span>, again again"
      //
      // STEP 3 - split
      //     ["", "<span>again</span>", " again again"]
      //
      // STEP 4 - loop and rewrite
      //     "<span>again</span><sup> again again</sup>"

      // mark selection
      const selectionTag = 'span'
      const selectionTagClass = 'ejs-active-text-selection'
      const selection = document.createElement(selectionTag)
      selection.classList.add(selectionTagClass)
      range.surroundContents(selection)

      // place a delimiter for splitting
      const searchKey = selection.outerHTML
      const htmlWithDelimiter = superscript.innerHTML.replace(searchKey, `,${searchKey},`)

      // split by delimiter
      let parts = htmlWithDelimiter.split(',')

      // remove empty string in array
      parts = parts.filter((el) => {
        return (el !== '')
      })

      // rebuild new HTML
      const searchKeyIdx = parts.indexOf(searchKey)
      const openingTag = `<${this.tag} class="${Superscript.cssClass}">`
      const closingTag = `</${this.tag}>`
      let newHtml = openingTag
      for (var i = 0; i < parts.length; i++) {
        if (parts[i] === '') {
          continue
        }

        let prefix = ''
        let suffix = ''
        const isSelection = (i === searchKeyIdx)

        if (isSelection) {
          if (i === 0) {
            // at the beginning
            newHtml = ''
            suffix = openingTag
          } else if (i === parts.length - 1) {
            // at the end
            prefix = closingTag
          } else {
            // in between
            prefix = closingTag
            suffix = openingTag
          }

          parts[i] = parts[i].replace(`<${selectionTag} class="${selectionTagClass}">`, '')
          parts[i] = parts[i].replace(`</${selectionTag}>`, '')
        } else {
          if (i === parts.length - 1) {
            suffix = closingTag
          }
        }

        newHtml += prefix + parts[i] + suffix
      }

      // convert html to nodes
      const newNodes = htmlToElements(newHtml)
      const nodesArray = Array.from(newNodes).reverse()

      const promises = nodesArray.map((el) => {
        return new Promise((resolve) => {
          insertAfter(el, superscript, resolve)
        })
      })

      Promise.all(promises).then(() => {
        superscript.remove()
      })
    }
  }

  checkState (selection) {
    try {
      if (this.isBrowserFirefox) {
        this.checkStateForFirefox(selection)
      }

      // if subscript, underline or strikethrough is enabled, then disable superscript button
      const subscript = this.api.selection.findParentTag('SUB', 'ejs-sub')
      const underline = this.api.selection.findParentTag('SPAN', 'u-ejs-underline')
      const strikethrough = this.api.selection.findParentTag('SPAN', 'u-ejs-strikethrough')
      this.button.disabled = !!(subscript || underline || strikethrough)

      const superscript = this.api.selection.findParentTag(this.tag, Superscript.cssClass)
      this.state = !!superscript
      this.updateButtonState()
    } catch (error) {
      if (error instanceof FirefoxSpecialCaseException === false) {
        console.error(error)
      }
    }
  }

  checkStateForFirefox (selection) {
    const anchorNode = selection.anchorNode
    const focusNode = selection.focusNode

    // selection in the beginning
    if (anchorNode.parentElement === focusNode.previousElementSibling &&
      anchorNode.parentElement.tagName === this.tag &&
      anchorNode.parentElement.classList.contains(Superscript.cssClass)) {
      this.api.selection.expandToTag(anchorNode.parentElement)
      this.state = true
      this.updateButtonState()
      throw new FirefoxSpecialCaseException('Selection in the beginning')
    }

    // selection in the middle
    if (anchorNode.nextElementSibling && focusNode.previousElementSibling &&
      anchorNode.nextElementSibling === focusNode.previousElementSibling &&
      anchorNode.nextElementSibling.tagName === this.tag &&
      anchorNode.nextElementSibling.classList.contains(Superscript.cssClass)) {
      this.api.selection.expandToTag(anchorNode.nextElementSibling)
      this.state = true
      this.updateButtonState()
      throw new FirefoxSpecialCaseException('Selection in the middle')
    }

    // selection in the end
    if (anchorNode.nextElementSibling === focusNode.firstElementChild &&
      anchorNode.nextElementSibling.tagName === this.tag &&
      anchorNode.nextElementSibling.classList.contains(Superscript.cssClass)) {
      this.api.selection.expandToTag(focusNode.firstElementChild)
      this.state = true
      this.updateButtonState()
      throw new FirefoxSpecialCaseException('Selection in the end')
    }

    // sometimes firefox's selection doesn't conform with the above rules,
    // so continue to fallback to default checkState() method
  }

  updateButtonState () {
    if (this.state) {
      this.button.classList.add('ce-inline-tool--active')
    } else {
      this.button.classList.remove('ce-inline-tool--active')
    }
  }
}

/**
 * Exception used to control flow of logic in Firefox browsers.
 * Selection API in Firefox works differently when compared to Chrome.
 * This affects EditorJS selection values.
 *
 * @param {String} message Error message
 */
function FirefoxSpecialCaseException (message) {
  this.name = 'Firefox Selection API Special Case'
  this.level = 'DEBUG'
  this.message = message
}

/**
 * Returns NodesList from the given HTML string
 * @param {String} html The HTML string
 * @refer https://stackoverflow.com/questions/494143/creating-a-new-dom-element-from-an-html-string-using-built-in-dom-methods-or-pro/35385518#35385518
 */
function htmlToElements (html) {
  const template = document.createElement('template')
  template.innerHTML = html
  return template.content.childNodes
}

/**
 * Inserts new node after the reference node
 * @param {Node} newNode New node to be inserted
 * @param {Node} referenceNode Reference node after which new node should be inserted
 * @param {CallableFunction} callback Callback function
 * @refer https://stackoverflow.com/questions/4793604/how-to-insert-an-element-after-another-element-in-javascript-without-using-a-lib
 */
function insertAfter (newNode, referenceNode, callback) {
  referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling)
  callback()
}
