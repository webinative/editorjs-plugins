# EditorJS plugins

EditorJS plugins for,

- strikethrough
- superscript
- subscript
- underline

## Usage

```javascript
import EditorJS from '@editorjs/editorjs'
import Paragraph from '@editorjs/paragraph'
import Superscript from '../editorjs-plugins/superscript/superscript'
import Subscript from '../editorjs-plugins/subscript/subscript'
import Strikethrough from '../editorjs-plugins/strikethrough/strikethrough'
import Underline from '../editorjs-plugins/underline/underline'

const editorjsTools = {
  paragraph: {
    class: Paragraph,
    inlineToolbar: ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough']
  },
  superscript: {
    class: Superscript
  },
  subscript: {
    class: Subscript
  },
  strikethrough: {
    class: Strikethrough
  },
  underline: {
    class: Underline
  }
}

const editor = new EditorJS({
  holder: 'my-codex-editor',
  tools: editorjsTools
})
```

## Known issues

Aware of the minor glitches in Firefox browser. To be fixed.
