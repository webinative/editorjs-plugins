import toolIcon from './ejs-underline.svg'

//
// refer https://editorjs.io/inline-tool-user-interaction example
//
export default class Underline {
  static get cssClass () {
    return 'u-ejs-underline'
  }

  constructor ({ api }) {
    this.api = api
    this.button = null

    this._state = false

    // tag must be UPPERCASE
    // NOT using U tag - unannotated text since HTML5
    this.tag = 'SPAN'

    this.isBrowserFirefox = false
    if (/Firefox\//.test(navigator.userAgent)) {
      this.isBrowserFirefox = true
    }
  }

  static get isInline () {
    return true
  }

  static get title () {
    return 'Underline'
  }

  static get sanitize () {
    return {
      span: {
        class: true
      }
    }
  }

  get shortcut () {
    return 'CTRL+U'
  }

  get state () {
    return this._state
  }

  set state (state) {
    this._state = state
  }

  render () {
    this.button = document.createElement('button')
    this.button.type = 'button'
    this.button.innerHTML = toolIcon
    this.button.classList.add('ce-inline-tool')

    return this.button
  }

  surround (range) {
    if (this.state) {
      this.unwrap(range)
      return
    }

    this.wrap(range)
  }

  wrap (range) {
    const selectedText = range.extractContents()
    const underline = document.createElement(this.tag)

    underline.classList.add(Underline.cssClass)
    underline.style.textDecoration = 'underline'
    underline.appendChild(selectedText)
    range.insertNode(underline)

    this.api.selection.expandToTag(underline)
  }

  unwrap (range) {
    const underline = this.api.selection.findParentTag(this.tag, Underline.cssClass)

    const tagContent = underline.innerText

    if (tagContent === range.toString()) {
      // full selection
      const text = range.extractContents()
      underline.remove()
      range.insertNode(text)
    } else {
      // partial selection

      // THOUGHT PROCESS
      //
      // underline text
      //     "again again again"
      //
      // STEP 1 - mark selection
      //     "<selection>again</selection> again again"
      //
      // STEP 2 - place delimiters for splitting
      //     ",<selection>again</selection>, again again"
      //
      // STEP 3 - split
      //     ["", "<selection>again</selection>", " again again"]
      //
      // STEP 4 - loop and rewrite
      //     "again<underline> again again</underline>"

      // mark selection
      const selectionTag = 'span'
      const selectionTagClass = 'ejs-active-text-selection'
      const selection = document.createElement(selectionTag)
      selection.classList.add(selectionTagClass)
      range.surroundContents(selection)

      // place a delimiter for splitting
      const searchKey = selection.outerHTML
      const htmlWithDelimiter = underline.innerHTML.replace(searchKey, `,${searchKey},`)

      // split by delimiter
      let parts = htmlWithDelimiter.split(',')

      // remove empty string in array
      parts = parts.filter((el) => {
        return (el !== '')
      })

      // rebuild new HTML
      const searchKeyIdx = parts.indexOf(searchKey)
      const openingTag = `<${this.tag} class="${Underline.cssClass}" style="text-decoration: underline">`
      const closingTag = `</${this.tag}>`
      let newHtml = openingTag
      for (var i = 0; i < parts.length; i++) {
        if (parts[i] === '') {
          continue
        }

        let prefix = ''
        let suffix = ''
        const isSelection = (i === searchKeyIdx)

        if (isSelection) {
          if (i === 0) {
            // at the beginning
            newHtml = ''
            suffix = openingTag
          } else if (i === parts.length - 1) {
            // at the end
            prefix = closingTag
          } else {
            // in between
            prefix = closingTag
            suffix = openingTag
          }

          parts[i] = parts[i].replace(`<${selectionTag} class="${selectionTagClass}">`, '')
          parts[i] = parts[i].replace(`</${selectionTag}>`, '')
        } else {
          if (i === parts.length - 1) {
            suffix = closingTag
          }
        }

        newHtml += prefix + parts[i] + suffix
      }

      // convert html to nodes
      const newNodes = htmlToElements(newHtml)
      const nodesArray = Array.from(newNodes).reverse()

      const promises = nodesArray.map((el) => {
        return new Promise((resolve) => {
          insertAfter(el, underline, resolve)
        })
      })

      Promise.all(promises).then(() => {
        underline.remove()
      })
    }
  }

  checkState (selection) {
    try {
      if (this.isBrowserFirefox) {
        this.checkStateForFirefox(selection)
      }

      // if superscript, subscript or strikethrough is enabled, then disable underline button
      const superscript = this.api.selection.findParentTag('SUP', 'ejs-sup')
      const subscript = this.api.selection.findParentTag('SUB', 'ejs-sub')
      const strikethrough = this.api.selection.findParentTag('SPAN', 'u-ejs-strikethrough')
      this.button.disabled = !!(superscript || subscript || strikethrough)

      const underline = this.api.selection.findParentTag(this.tag, Underline.cssClass)
      this.state = !!underline
      this.updateButtonState()
    } catch (error) {
      if (error instanceof FirefoxSpecialCaseException === false) {
        console.error(error)
      }
    }
  }

  checkStateForFirefox (selection) {
    const anchorNode = selection.anchorNode
    const focusNode = selection.focusNode

    // selection in the beginning
    if (anchorNode.parentElement === focusNode.previousElementSibling &&
        anchorNode.parentElement.tagName === this.tag &&
        anchorNode.parentElement.classList.contains(Underline.cssClass)) {
      this.api.selection.expandToTag(anchorNode.parentElement)
      this.state = true
      this.updateButtonState()
      throw new FirefoxSpecialCaseException('Selection in the beginning')
    }

    // selection in the middle
    if (anchorNode.nextElementSibling && focusNode.previousElementSibling &&
      anchorNode.nextElementSibling === focusNode.previousElementSibling &&
      anchorNode.nextElementSibling.tagName === this.tag &&
      anchorNode.nextElementSibling.classList.contains(Underline.cssClass)) {
      this.api.selection.expandToTag(anchorNode.nextElementSibling)
      this.state = true
      this.updateButtonState()
      throw new FirefoxSpecialCaseException('Selection in the middle')
    }

    // selection in the end
    if (anchorNode.nextElementSibling === focusNode.firstElementChild &&
      anchorNode.nextElementSibling.tagName === this.tag &&
      anchorNode.nextElementSibling.classList.contains(Underline.cssClass)) {
      this.api.selection.expandToTag(focusNode.firstElementChild)
      this.state = true
      this.updateButtonState()
      throw new FirefoxSpecialCaseException('Selection in the end')
    }

    // sometimes firefox's selection doesn't conform with the above rules,
    // so continue to fallback to default checkState() method
  }

  updateButtonState () {
    if (this.state) {
      this.button.classList.add('ce-inline-tool--active')
    } else {
      this.button.classList.remove('ce-inline-tool--active')
    }
  }
}

/**
 * Exception used to control flow of logic in Firefox browsers.
 * Selection API in Firefox works differently when compared to Chrome.
 * This affects EditorJS selection values.
 *
 * @param {String} message Error message
 */
function FirefoxSpecialCaseException (message) {
  this.name = 'Firefox Selection API Special Case'
  this.level = 'DEBUG'
  this.message = message
}

/**
 * Returns NodesList from the given HTML string
 * @param {String} html The HTML string
 * @refer https://stackoverflow.com/questions/494143/creating-a-new-dom-element-from-an-html-string-using-built-in-dom-methods-or-pro/35385518#35385518
 */
function htmlToElements (html) {
  const template = document.createElement('template')
  template.innerHTML = html
  return template.content.childNodes
}

/**
 * Inserts new node after the reference node
 * @param {Node} newNode New node to be inserted
 * @param {Node} referenceNode Reference node after which new node should be inserted
 * @param {CallableFunction} callback Callback function
 * @refer https://stackoverflow.com/questions/4793604/how-to-insert-an-element-after-another-element-in-javascript-without-using-a-lib
 */
function insertAfter (newNode, referenceNode, callback) {
  referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling)
  callback()
}
